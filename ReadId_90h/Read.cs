﻿using NAND_Prog;
using System.ComponentModel.Composition;

namespace ReadId_90h
{

    [Export(typeof(Operation)),
        ExportMetadata("Name", "ReadId_90h")]
    public class Read : AbstractRead
    {
        public Read()
        {
            name = "Read";
            based = typeof(Register);

            Instruction instruction;
            //--------------------------------------------------------------
            instruction = new WriteCommand();                              //Створюю інструкцію WriteCommand
            chainInstructions.Add(instruction);                            //Додаю її в ланцюжок інструкцій операції Read

            //    instruction.numberOfcycles = 0x01;                           //По дефолту 1       
            (instruction as WriteCommand).command = 0x90;
            (instruction as WriteCommand).Implementation = GetCommandMG;
            //--------------------------------------------------------------
            //--------------------------------------------------------------

            instruction = new WriteAddress();
            chainInstructions.Add(instruction);

            //instruction.numberOfcycles = 1;                                    ////По дефолту 1  
            //(instruction as WriteAddress).address = 0x00;                      //задаю статичну адресу . Не треба вказувати -в методі GetAddressMG  адреса буде братись з client-а 
            (instruction as WriteAddress).Implementation = GetAddressMG;
            //--------------------------------------------------------------           
            //--------------------------------------------------------------

            instruction = new ReadData();
            chainInstructions.Add(instruction);
            (instruction as ReadData).Implementation = GetDataMG;
            //--------------------------------------------------------------
        }

        private BufMG GetDataMG(ChipPart client)                        //Реалізація _implementation для  ReadData : Instruction
        {
            DataMG meneger = new DataMG();
            meneger.direction = Direction.In;    //дані на вхід

            meneger.SetDataPlace((client as IDatable).GetDataPlace());
            meneger.numberOfCycles = ((client as IDatable).GetDataSize());
            return meneger;

        }



    }
}
